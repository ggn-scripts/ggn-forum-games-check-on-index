// ==UserScript==
// @name        GGn Forum Games Check on Index
// @namespace   Violentmonkey Scripts
// @match       https://gazellegames.net/forums.php
// @grant       GM.getValue
// @grant       GM.setValue
// @version     1.0
// @author      ScoobieDee
// @description 10/20/2020, 9:38:44 PM
// ==/UserScript==
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
if(urlParams.get('forumid') == "55"){
  forum_game_index = 1;
} else {
  forum_game_index = 0;
}

function setCheckThreadCountFlag() {
  (async () => {
    await GM.setValue("check_threadcount", 1);
  })();
}


if(forum_game_index) {
  forum_index = document.getElementsByClassName("forum_index");
  (async () => {
    for (var i = 0, row; row = forum_index[0].rows[i]; i++) {
      //iterate through rows
      a_tags = row.cells[1].getElementsByTagName("A");
      if(a_tags.length > 0) {
        thread_id_regex = /threadid=(.*)/;
        thread_id = thread_id_regex.exec(a_tags[0])[1];
        reply_count = parseInt(row.cells[2].textContent.replace(/,/g, ""));
        let my_last_reply = await GM.getValue(thread_id, 0);
        // console.log('Thread: ', thread_id, ', Reply count: ', reply_count, ' My Last Reply: ', my_last_reply);
        if(my_last_reply != 0){
          if(reply_count >= my_last_reply+3) {
            // console.log('Set background color to green')
            row.style.backgroundColor = "green"
          } else {
            row.style.backgroundColor = "red"
          }
        }
      }
    }
  })();
} else {
  var content = document.querySelector('div#content');
  var check = content.querySelector('div.thin > h2 > a');
  if(check !== null){
    var check2 = check.nextElementSibling;
    if(check2 !== null){
      var check3 = check2.text;
      if(check3 === 'Forum Games'){
        const thread_id = urlParams.get('threadid')
        var S=document.querySelectorAll('input[type=submit]');
        for (var i = 0, subbut; subbut = S[i]; i++) {
          if(subbut.value == "Submit reply") {
            subbut.addEventListener("click", setCheckThreadCountFlag)
          }
        }
        //Check for the Check Threadcount Flag when loading the page.
        (async () => {
          let check_threadcount = await GM.getValue("check_threadcount", 0);
          // console.log('Check Threadcount? ', check_threadcount);
          if(check_threadcount) {
            // console.log('Check index for post count and record it.');
            var ajax = new XMLHttpRequest();
            ajax.open('get','https://gazellegames.net/forums.php?action=viewforum&forumid=55');
            ajax.responseType = 'document';
            ajax.onload = function(){
                forum_index = ajax.responseXML.getElementsByClassName("forum_index"); //And this is a document which may execute getElementById
                if(forum_index.length == 1) {
                  // Iterate through each table row to get the thread id.
                  (async () => {
                    for (var i = 0, row; row = forum_index[0].rows[i]; i++) {
                      //iterate through rows
                      a_tags = row.cells[1].getElementsByTagName("A");
                      if(a_tags.length > 0) {
                        thread_id_regex = /threadid=(.*)/;
                        thread_id_to_check = thread_id_regex.exec(a_tags[0])[1];
                        if(thread_id_to_check == thread_id) {
                          reply_count = parseInt(row.cells[2].textContent.replace(/,/g, ""));
                          // console.log('This thread has this many replies: ', reply_count)
                          await GM.setValue(thread_id, reply_count);
                          await GM.setValue("check_threadcount", 0);
                        }
                      }
                    }
                  })();
                }
            };
            ajax.send();
          }
        })();
      }
    }
  }
}
